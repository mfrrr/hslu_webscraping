install.packages("RSelenium")
install.packages("stringr")
install.packages("rvest")
library(RSelenium)
library(stringr)
library(rvest)

######## SET UP CONNECTION TO BROWSER & BROWSE TO PAGE ########
###############################################################

#Establish connection to browser via web driver 
remoteDriver <- RSelenium::rsDriver(port = 6451L, browser = "chrome", chromever = "98.0.4758.102")
remoteDriverClient <- remoteDriver[["client"]]

#OR
#remoteDriver$open()

######## WEB SCRAPING #########
###############################

#get to first page of search results
remoteDriverClient$navigate("http://www.autoscout24.ch/de/autos/alle-marken?fuel=16&cond=49&make=-1&vehtyp=10")

#get the number of pages of search results
pageEnd <- remoteDriverClient$findElements(using = "class", value = "page-item")
vector <- vector()
for (element in pageEnd) {
  detail <- element$getElementAttribute("innerHTML")[[1]]
  vector <- c(vector, detail)
}
pageEndFinal <- tail(vector, n = 1)
pageEndFinal <- substr(pageEndFinal, 41, 43)
pageEndFinal

rowCount <- 0

#iterate through every page of the search results
for (page in 1:pageEndFinal) {
  autoscoutURL <- str_glue("https://www.autoscout24.ch/de/autos/alle-marken?fuel=16&cond=49&make=-1&page={page}&vehtyp=10")
  remoteDriverClient$navigate(autoscoutURL)

  #read redirects
  redirects <- remoteDriverClient$findElements("css", ".base-nav-link.stretched-link")
  listRedirects = list()
  for (e in redirects) {
    redirectText <- e$getElementAttribute("outerHTML")[[1]]
    listRedirects <- append(listRedirects, redirectText)
  }
  #listRedirects

  #get the links
  listLinks = list()
  for (e in listRedirects) {
    linkSplit1 <- scan(text = e, what = "")[5]
    linkSplit2 <- list(strsplit(linkSplit1, split = "\""))
    linkSplitFinal <- linkSplit2[[1]][[1]][[2]]
    listLinks <- append(listLinks, linkSplitFinal)
  }
  #listLinks

  #iterate through all advertisements and scrape data
  df_scraped <- data.frame(carID = character(0), carName = character(0), carPrice = character(0), carKM = character(0), carPS = character(0),
                           carRange = character(0), carBatteryCapacity = character(0), carEnergyConsumption = character(0))
  lengthList <- length(listLinks)
  for (e in 1:lengthList) {
    addLink <- listLinks[[e]]
    pdpURL <- str_glue("https://www.autoscout24.ch{addLink}")
    
    #get carID
    carID <- strsplit(listLinks[[e]], "vehid=")[[1]][2]
    carID <- strsplit(carID, "&")[[1]][1]
    carID
    
    sleepTime <- runif(1, min = 1, max = 5)
    Sys.sleep(sleepTime)
    remoteDriverClient$navigate(pdpURL)
    Sys.sleep(1)
  
    #get name of the car
    carName <- remoteDriverClient$findElement(using = "css selector", value = "h1")
    carNameText <- carName$getElementText()
  
    #get price of the car
    carPrice <- remoteDriverClient$findElement(using = "class", value = "h1")
    carPriceText <- carPrice$getElementText()
  
    #get reach + battery capacity 
    electro_details <- remoteDriverClient$findElements(using = "class", value = "electro-data__value")
    vector <- vector()
    for (element in electro_details) {
      detail <- element$getElementAttribute("innerHTML")[[1]]
      vector <- c(vector, detail)
    }
    carRangeText <- vector[1]
    carBatteryCapacityText <- vector[2]
    
    #get car details
    car_details <- remoteDriverClient$findElements(using = "class", value = "text-ellipsis")
    vector_car_details <- vector()
    for (element in car_details) {
      detail <- element$getElementAttribute("innerHTML")[[1]]
      vector_car_details <- c(vector_car_details, detail)
    }
    
    #get KM driven
    carKMdrivenText <- vector_car_details[2]
    #get PS
    carPSText <- vector_car_details[3]
    
    #get energy consumption
    carEnergyConsumptionText1 <- vector_car_details[length(vector_car_details)]
    carEnergyConsumptionText2 <- vector_car_details[length(vector_car_details)-1]
    if (grepl("kWh/100 km", carEnergyConsumptionText1, fixed = TRUE)) {
      carEnergyConsumptionText <-  carEnergyConsumptionText1
    } else if (grepl("kWh/100 km", carEnergyConsumptionText2, fixed = TRUE)) {
      carEnergyConsumptionText <- carEnergyConsumptionText2
    } else {
      carEnergyConsumptionText <- "NA"
    }
    
    #collect scrapped data per advertisement in vector
    vectorTextRow <- c(carID, carNameText, carPriceText, carKMdrivenText, carPSText, carRangeText, carBatteryCapacityText, carEnergyConsumptionText)
  
    #add scrapped data to data frame
    df_scraped[nrow(df_scraped) + 1,] = vectorTextRow
    rowCount <- rowCount + 1
    print(rowCount)
    Sys.sleep(1)
  
  }
}



